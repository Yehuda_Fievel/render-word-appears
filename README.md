## Summary

Create a small web application that ranks the words on a given wikipedia topic

 

## Details

1. Create a simple webpage using a framework or vanilla javascript.
2. Create a search input for the search topic
3. Call the Wiki API with the URL below 
https://en.wikipedia.org/w/api.php?action=query&prop=extracts&format=json&exintro=&titles=NASA&format=json&callback=callback

Extract the text from the response (from the “extract” property)

 

In a table or list:  
- The text words (with ranks) sorted by occurrence rank (descending order) 
- Ranks may have values from 1 to 5. Most common word has rank 5 
- Words with the same rank sorted alphanumerically by ascending order
- Rank presented as number of stars - 5: *****, 4: ****, etc.  
- Note that we are not looking for the count of the words but word’s rank, meaning for example that the highest appearance in the text should get 5 stars

Make sure you have clean code and unit tests
 

## Example

Input text: “car bicycle car bicycle car bicycle car bicycle car bicycle car bicycle plane plane truck”

Output

Bicycle (*****) 

Car (*****) 

Plane (**) 

Truck (*)