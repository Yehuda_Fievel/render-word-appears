import { useState } from "react";
import axios from "axios";

const App = () => {
  const [value, setValue] = useState("");
  const [stars, setStars] = useState([]);

  const onChange = (e) => {
    e.preventDefault();
    setValue(e.target.value);
  };

  const onClick = async () => {
    const response = await axios(
      `https://en.wikipedia.org/w/api.php?action=query&prop=extracts&format=json&exintro=&titles=${value}&format=json&callback=callback&origin=*`
    );
    const json = JSON.parse(
      response.data.replace("/**/callback(", "").slice(0, -1)
    );
    const pages = json.query.pages;
    const html = pages[Object.keys(pages)].extract;
    const text = html
      .replace(/<[^>]*>?/gm, "")
      .replace(/[.,/#!$%^&*;:{}=\-_`~()'"]/g, "");

    const arr = text.trim().split(" ");
    const obj = {};

    for (let i = 0; i < arr.length; i++) {
      const word = arr[i].toLowerCase();
      obj[word] = obj[word] ? obj[word] + 1 : 1;
    }

    const values = Object.entries(obj).sort((a, b) => b[1] - a[1]);
    values.length = 5;

    console.log(values)

    setStars(values);
  };

  return (
    <>
      <input data-testid="input" type="text" value={value} onChange={onChange} />
      <button data-testid="btn" onClick={onClick}>
        Search
      </button>
      <br />
      <br />
      <div data-testid="stars">
        {stars.map((input, index) => {
          let line = `${input[0]} : `;
          for (let i = 5 - index; i > 0; i--) {
            line += "*";
          }
          return <div data-testid={line-`${index}`}>{line}</div>;
        })}
      </div>
    </>
  );
};

export default App;
