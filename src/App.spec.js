import { render, fireEvent, screen } from "@testing-library/react";
import App from "./App";

//test block
test("search", () => {
  render(<App />);

  const input = screen.getByTestId("input");
  const button = screen.getByTestId("btn");

  fireEvent.change(input, { target: { value: "NASA" } });
  expect(input.value).toBe("NASA");

  fireEvent.click(button);
});
